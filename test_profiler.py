import pandas
import numpy as nu
from src.profiler import *

def testGetUniqueMembers():
	"""
	Example
	---------------
	df 
	 		Date 		DimA 		DimB 		DimC 		Measure 
	index 
	0       20150101 	Category A 	Product A 	Location A 	1000
	1 		20150102 	Category A 	Product B 	Location B 	1000
	2 		20150103 	Category A 	Product C 	Location A 	1000
	3  		20150104 	Category B 	Product D 	Location B 	1000
	4       20150105 	Category B 	Product E 	Location C 	1000
	"""
	inputData = {
	'Date' : pandas.Series(['20150101','20150102','20150103','20150104','20150105'], index=[0,1,2,3,4]),
	'DimA' : pandas.Series(['Category A', 'Category A', 'Category A', 'Category B', 'Category B'], index=[0,1,2,3,4]),
	'DimB' : pandas.Series(['Product A', 'Product B', 'Product C', 'Product D', 'Product E'], index=[0,1,2,3,4]),
	'DimC' : pandas.Series(['Location A', 'Location B', 'Location A', 'Location B', 'Location C'], index=[0,1,2,3,4]),
	'Measure' : pandas.Series([1000, 1000, 1000, 1000, 1000], index=[0,1,2,3,4])
	}
	inputDataFrame = pandas.DataFrame(inputData) 
	result = getUniqueMembers(inputDataFrame, ["DimA", "DimB", "DimC"])
	expectedResult = [
		{'count': 2, 'dimension': 'DimA', 'members': np.array(['Category A', 'Category B'], dtype=object)}, 
		{'count': 5, 'dimension': 'DimB', 'members': np.array(['Product A', 'Product B', 'Product C', 'Product D', 'Product E'], dtype=object)}, 
		{'count': 3, 'dimension': 'DimC', 'members': np.array(['Location A', 'Location B', 'Location C'], dtype=object)}
	]
	assert (result[0]["members"] == expectedResult[0]["members"]).all()
	assert (result[1]["members"] == expectedResult[1]["members"]).all()
	assert (result[2]["members"] == expectedResult[2]["members"]).all()
	assert result[0]["count"] == expectedResult[0]["count"]
	assert result[1]["count"] == expectedResult[1]["count"]
	assert result[2]["count"] == expectedResult[2]["count"]
	
