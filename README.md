# Description #

This is the example code for AI4BI's module Profiler including getUniqueMembers function.

## Quest ##

Complete getDimensionHierarchies function which returns list of dimensions with their hierarchies. 
See function description for more details and example.

## Tests ##

Tests using py.test are in test_profile.py. 






