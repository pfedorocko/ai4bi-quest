import pandas as pd 
import numpy as np

def getUniqueMembers(df, dimensions):
	"""
	Function creates list of dicts with unique members and their count for every dimension.
	@params  df 			pandas dataframe
	@params  dimensions		list of dimension names
	@returns members 		list of dics {dimension, members}
	
	Example
	---------------
	df 
	 		Date 		DimA 		DimB 		DimC 		Measure 
	index 
	0       20150101 	Category A 	Product A 	Location A 	1000
	1 		20150102 	Category A 	Product B 	Location B 	1000
	2 		20150103 	Category A 	Product C 	Location A 	1000
	3  		20150104 	Category B 	Product D 	Location B 	1000
	4       20150105 	Category B 	Product E 	Location C 	1000
	
	dimensions = ["DimA", "DimB", "DimC"]
	members = [
		{"dimension": "DimA", "members": ["Category A", "Category B"], "count": 2},
		{"dimension": "DimB", "members": ["Product A","Product B", "Product C", "Product D", "Product E", ], "count": 5},
		{"dimension": "DimC", "members": ["Location A", "Location B", "Location C"], "count": 3}
	]
	"""
	members = [] #array keeping dics of dimension name and its unique values
	for dim in dimensions:
		try:
			members.append({"dimension": dim, "members": np.unique(df[[dim]]), "count": len(np.unique(df[[dim]]))})
		except Exception as e:
			print "Data frame has no column " + dim + "Error:" + str(e)
	print "DONE: Unique dimensions' members has been obtained."		
	return members	

def getDimensionHierarchies(df, dimensions):
	"""
	Function creates list of dicts with dimensions and their childrens.
	@params  df 			pandas dataframe
	@params  dimensions		list of dimension names
	@returns hierarchy 		list of dics {dimension, members}
	
	Example
	---------------
	df 
	 		Date 		DimA 		DimB 		DimC 		Measure 
	index 
	0       20150101 	Category A 	Product A 	Location A 	1000
	1 		20150101 	Category A 	Product B 	Location B 	1000
	2 		20150101 	Category A 	Product C 	Location A 	1000
	3  		20150101 	Category B 	Product D 	Location B 	1000
	4       20150101 	Category B 	Product E 	Location C 	1000
	
	dimensions = ["DimA", "DimB", "DimC"]
	hierarchy = [
		{"dimension": "DimA", "child": "DimB"},
		{"dimension": "DimB"},
		{"dimension": "DimC"}
	]
	"""
	pass